# REAct

> A static website powered by [React.js](http://facebook.github.io/react/)
> and [Webpack](http://webpack.github.io/).

**Note**: *This project is based on [react-static-boilerplate](https://github.com/koistya/react-static-boilerplate).*

### Plan
* Use react since I have more experience with it then Angular.
* Start with the ui, and a mockup of states, then worry about data & TDD
* Will start with three components;

1. A **Page** pages/index.js component that will be responsible for loading the data and sendings as props to two collections, one for listings, one for saved listings.
* States;
    * *hasLoaded* : boolean : false

2. **collection** components/Property__collection.js which will be used to display all Property items

* Props;
    *cards* object? tbc
    *collectionType* string

3. **card** components/Property__card.js
* States;
   * *isHovering*: boolean : false ! don't think i'll need this after all... i'll see how this goes controlling visibility through css alone
   * *isSaved*: boolean : false - can be us
   * *results*: object : results from request
   * *saved*: object : saved results from request
* Props;
   * *agentLogo*: string json.results.agency.logo
   * *agentColorPrimary*: string json.results.agency.brandingColors.primary
   * *imagePrimary* string json.results.mainImage
   * *price* string json.results.price
   * *uid* number json.results.id

- I used lodash to for object helpers, could have used immutablejs if there was more complexity
- To test; `npm test`
- The unit test setup for this project did not work for me out of the box, I did some quick looking around but decided to pretty much use this as an example https://github.com/robertknight/react-testing, however;
    * For a product I would prefer to refine this testing and sort out a confident solution. Preferably with something that is accessible in the browser like robert knights example, as i'm getting a warning in the console 'Warning: ReactDOMComponent: Do not access .props of a DOM node; instead, recreate the props as `render` did originally or read the DOM properties/attributes directly from this node (e.g., this.refs.box.className). This DOM node was rendered by `PropertyCard`.'.

**Note**: * Below is documentation that comes with react-static-boilerplate*

### Features

&nbsp; &nbsp; ✓ Generates static `.html` pages from [React](http://facebook.github.io/react/) components<br>
&nbsp; &nbsp; ✓ Generates routes based on the list of files in the `/pages` folder<br>
&nbsp; &nbsp; ✓ Next generation JavaScript with [Babel](https://github.com/babel/babel)<br>
&nbsp; &nbsp; ✓ [Sass](http://sass-lang.com/) syntax for CSS via [postCSS](https://github.com/postcss/postcss) and [precss](https://github.com/jonathantneal/precss)<br>
&nbsp; &nbsp; ✓ Development web server with [BrowserSync](http://www.browsersync.io) and [React Transform](https://github.com/gaearon/babel-plugin-react-transform)<br>
&nbsp; &nbsp; ✓ Bundling and optimization with [Webpack](http://webpack.github.io/)<br>
&nbsp; &nbsp; ✓ [Code-splitting](https://github.com/webpack/docs/wiki/code-splitting) and async chunk loading<br>
&nbsp; &nbsp; ✓ Easy deployment to [GitHub Pages](https://pages.github.com/), [Amazon S3](http://davidwalsh.name/hosting-website-amazon-s3) or [Firebase](https://www.firebase.com/)<br>
&nbsp; &nbsp; ✓ [Yeoman](http://yeoman.io/) generator ([generator-react-static](https://www.npmjs.com/package/generator-react-static))<br>

### Directory Layout

```
.
├── /build/                     # The folder for compiled output
├── /node_modules/              # 3rd-party libraries and utilities
├── /components/                # React components
├── /lib/                       # Libraries and utilities
├── /pages/                     # React.js-based web pages
│   ├── /blog/                  # Blog post entries example
│   ├── /404.js                 # Not Found page
│   ├── /500.js                 # Error page
│   ├── /about.js               # About Us page
│   └── /index.js               # Home page
├── /static/                    # Static files such as favicon.ico etc.
├── /test/                      # Unit and integration tests
├── /tools/                     # Build automation scripts and utilities
│── app.js                      # The main JavaScript file (entry point)
│── config.js                   # Website configuration / settings
│── package.json                # Dev dependencies and NPM scripts
└── README.md                   # Project overview
```

### Getting Started

Just clone the repo, install Node.js modules and run `npm start`:

```
$ git clone -o react-static-boilerplate -b master --single-branch \
      https://github.com/koistya/react-static-boilerplate.git MyApp
$ cd MyApp
$ npm install
$ npm start
```

Then open [http://localhost:3000/](http://localhost:3000/) in your browser.

### How to Test

The unit tests are powered by [chai](http://chaijs.com/) and [mocha](http://mochajs.org/).

```
$ npm test
```

### How to Deploy

```shell
$ npm run deploy                # Deploys the project to GitHub Pages
```

### Related Projects

  * [React Starter Kit](https://github.com/kriasoft/react-starter-kit)
  * [React Routing](https://github.com/kriasoft/react-routing)
  * [React Decorators](https://github.com/kriasoft/react-decorators)

### Learn More

  * [Getting Started with React.js](http://facebook.github.io/react/)
  * [Getting Started with GraphQL and Relay](https://quip.com/oLxzA1gTsJsE)
  * [React.js Questions on StackOverflow](http://stackoverflow.com/questions/tagged/reactjs)
  * [React.js Discussion Board](https://discuss.reactjs.org/)
  * [Learn ES6](https://babeljs.io/docs/learn-es6/), [ES6 Features](https://github.com/lukehoban/es6features#readme)

---
Copyright (c) Ben Rhodes.&nbsp; All rights reserved.
