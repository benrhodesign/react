import React, { Component, PropTypes } from 'react';
import './Layout.scss';

class Layout extends Component {

  render() {
    return (
      <div className="Layout">
        {this.props.children}
      </div>
    );
  }

}

export default Layout;
