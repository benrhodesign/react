import React, { Component, PropTypes } from 'react';
import './PropertyCard.scss';

class PropertyCard extends Component {
  static propTypes = {
    agentLogo: PropTypes.string.isRequired,
    imagePrimary: PropTypes.string.isRequired,
    uid: PropTypes.string.isRequired,
    agencyName: PropTypes.string,
    agentColorPrimary: PropTypes.string,
    imagePrimaryAlt: PropTypes.string,
    price: PropTypes.string,
    saved: PropTypes.bool
  };

  _handleChange = (i) => {
    this.props.onChange(i);
  };

  componentWillReceiveProps (newProps) { }

  render() {
    const buttonText = (this.props.saved) ? 'Remove article' : 'Save article';
    const buttonVariationClass = (this.props.saved) ? 'Card__button Card__button--remove' : 'Card__button Card__button--add';
    const scope = this;

    return (
      <div className="Card" ref="uid" data-id={"Card__"+this.props.uid}>
        <div className="Card__header" data-agent-col={this.props.agentColorPrimary} ref="agentColorPrimary" style={(this.props.agentColorPrimary) ? { background: this.props.agentColorPrimary } : {}}>
          <img ref="agentLogo" src={this.props.agentLogo} alt={this.props.agencyName} />
        </div>
        <figure className="Card__media">
          <img ref="imagePrimary" width="400" src={this.props.imagePrimary} alt="{this.props.imagePrimaryAlt}" />
          <button className={buttonVariationClass} onClick={scope._handleChange.bind(this, scope.props.uid)}>{buttonText}</button>
        </figure>
        <div className="Card__woff">
            <span className="Card__tag" ref="price">Price: {this.props.price}</span>
        </div>
      </div>
    );
  }

}

PropertyCard.defaultProps = {
  price: 'Contact Agent',
  agencyName: 'Real estate agency',
  imagePrimaryAlt: 'Property',
  saved: false
};

export default PropertyCard;