import React, { Component, PropTypes } from 'react';
import './PropertyCollection.scss';
import PropertyCard from '../PropertyCard';

class PropertyCollection extends Component {
  constructor (props) {
    super(props);
  };

  componentWillReceiveProps (newProps) { }

  render() {
    let variation = (this.props.collectionType == '') ? 'btn--remove' : 'btn--add';
    let title = 'Properties';
    switch (this.props.collectionType) {
      case 'saved':
        variation = 'PropertyCollection--saved';
        title = 'Saved Properties'
        break;
    }

    const scope = this;
    const nocards = <li className="PropertyCollection__empty">You have no saved properties.</li>;
    const cards = this.props.cards.map(function (card, i) {
      return (
        <li className="PropertyCollection__item" key={card.id}>
          <PropertyCard
            price={card.price}
            agentLogo={card.agency.logo}
            agentColorPrimary={card.agency.brandingColors.primary}
            imagePrimary={card.mainImage}
            saved={(scope.props.collectionType == 'saved')}
            uid={card.id}
            onChange={scope.props.onChange}
          />
        </li>
      );
    });

    return (
      <div className={'PropertyCollection ' + variation}>
        <header>
          <h2>{title}</h2>
        </header>
        <ul className="PropertyCollection__list">

          {(this.props.cards.length) ? cards : nocards}
        </ul>
      </div>
    );
  }

}

PropertyCollection.defaultProps = {
  cards: [],
  collectionType: 'save article',
  onChange: null
};

export default PropertyCollection;