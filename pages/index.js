import React, { Component } from 'react';
import PropertyCollection from '../components/PropertyCollection';

const _ = require('lodash');

class IndexPage extends Component {
  constructor (props) {
    super(props);
    this.state = {
      results: [],
      saved: [],
      hasLoaded: false
    };
  };

  _loadData(url) {

    return new Promise(
      function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
          if (xhr.readyState === XMLHttpRequest.DONE) {
            if (this.status === 200) {
              resolve(this.response);
            } else {
              reject('onreadyfails');
            }
          }
        }
        xhr.onerror = function () {
          reject('XMLHttpRequest Error: ');
        };

        xhr.open('GET', url, true);
        xhr.send();
      }
    );
  };

  _removeSavedItem = (i) => {
    var curr = this.state.saved;
    _.remove(curr, {
        id: i
    });
    this.setState({
      saved: curr
    });
  };

  _saveItem = (i) => {
    var selected =  _.find(this.state.results, { 'id': i });
    var currentsaved = this.state.saved;
    var newsaved = currentsaved;
    newsaved.push(selected);
    newsaved = _.uniqBy(currentsaved, 'id');

    //if currentsaved != newsaved then update state
    if(currentsaved != newsaved) {
      this.setState({
        saved: newsaved
      });
    }
  };

  componentWillReceiveProps (newProps) { };

  componentDidMount () {
    const { isScriptLoaded, isScriptLoadSucceed } = this.props;

    let self = this;
    self._loadData(self.props.data)
    .then(
      (value) => {
        let data = JSON.parse(value);
        self.setState({
          results: data['results'],
          saved: data['saved'],
          hasLoaded: true
        });
      },
      (reason) => {
        console.log('load failed : '+ reason);
      });
  };

  render() {
    return (
      <div className="grid">
        <div className="grid__row">
            <div className="jumpbotron">
              <h1>Page Title</h1>
            </div>
        </div>
        <div className="grid__row">
          <div className="grid__col-1-3">
            <PropertyCollection cards={this.state.results} onChange={this._saveItem.bind(this)} />
          </div>
          <div className="grid__col-1-3">
            <PropertyCollection collectionType="saved" cards={this.state.saved} onChange={this._removeSavedItem.bind(this)} />
          </div>
          <div className="grid__col-1-3 is-loading">
            <PropertyCollection cards={this.state.results} onChange={this._saveItem.bind(this)} />
            <p>force view of is-loading for style guide</p>
          </div>
        </div>
      </div>
    );
  }

}

IndexPage.defaultProps = {
  data: 'test-data.json'
};

export default IndexPage;
