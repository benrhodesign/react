//Created unit test based on examples found here https://github.com/robertknight/react-testing
import React from 'react';
import {renderIntoDocument} from 'react-addons-test-utils';
import 'ignore-styles';
import {expect} from 'chai';

import setup from './setup';

import PropertyCard from '../components/PropertyCard';

var rewire = require('rewire');

const TEST_CARD = {
  price: "$726,500",
  agency: {
    brandingColors: {
      primary: "#ffe512"
    },
    logo: "http://i1.au.reastatic.net/agencylogo/XRWXMT/12/20120927204448.gif"
  },
  id: "1",
  mainImage: "http://i2.au.reastatic.net/640x480/20bfc8668a30e8cabf045a1cd54814a9042fc715a8be683ba196898333d68cec/main.jpg"
};

describe('PropertyCard', () => {
  it('should display card details', () => {
    const card = TEST_CARD;
    const item = renderIntoDocument(
      <PropertyCard
        price={card.price}
        agentLogo={card.agency.logo}
        agentColorPrimary={card.agency.brandingColors.primary}
        imagePrimary={card.mainImage}
        uid={card.id}
      />
    );
    const price = item.refs.price;
    const uid = item.refs.uid;
    const agentLogo = item.refs.agentLogo;
    const agentColorPrimary = item.refs.agentColorPrimary;
    const imagePrimary = item.refs.imagePrimary;

    //test that price text is rendered correctly
    expect(price.textContent).to.equal('Price: ' + card.price);
    expect(uid.props['data-id']).to.equal('Card__' + card.id);
    expect(agentLogo.props['src']).to.equal(card.agency.logo);
    expect(imagePrimary.props['src']).to.equal(card.mainImage);
    expect(agentColorPrimary.props['data-agent-col']).to.equal(card.agency.brandingColors.primary);
  });
});